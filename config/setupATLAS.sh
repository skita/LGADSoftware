export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase 
alias setupATLAS='source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh'
setupATLAS --quiet 
#localSetupROOT   --quiet 
lsetup "root 6.14.04-x86_64-slc6-gcc62-opt"    --quiet 
lsetup cmake --quiet 
lsetup git --quiet 
