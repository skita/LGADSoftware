#!/bin/bash

if [ ! $1 ]
then
    echo "usage : $0 [runfile]"
fi

echo "runfile : " $1 
ROOTFILENAME="../"`grep output $1 | awk '{print $2}'`
SUFIX=`basename $1`

echo "root -l \"mktimereso.C(\"${ROOTFILENAME}\",\" ${SUFIX}\")\""
root -l "mktimereso.C(\"${ROOTFILENAME}\",\" ${SUFIX}\")"
