#ifndef _FILEREAD_HH
#define _FILEREAD_HH

#include <iomanip>
#include "UserInterFace.hh"
#include "TChain.h"
//#include "Common/header.hh"

class FileRead {
private:
  char buff[1024];

public:
  FileRead(){}
  ~FileRead(){}
  //  int SetChain(TChain* chain, Int_t Sample,Int_t Nfiles=50);
  int SetChain(TChain* chain, UserInterFace ui, std::string treename);
};

#endif // #ifndef _FILEREAD_HH
