#include <iostream>
#include <string>
#include <vector>
#include "UserInterFace.hh"
#include "InputInfo.h"
#include "HistogramVariableStrage.hh"
#include "PulseHeight.h"
#include "TCanvas.h"

class LGADLoopManager{
private:
  UserInterFace ui;
  bool debug;
  bool debug2;
  int eveprint;
  int startevent;
  int numevent;
  bool isStrip;
  bool isPad;
  double cutth1;
  double cutth2;
  std::vector<std::string> filelist;
  std::vector<std::string> flashadcnum;
  std::vector<int> sensorchnum;
  std::vector<int> ampchnum;
  std::vector<int> layernum;
  std::vector<double> offsetlist;
  std::vector<double> timeoffset;
  std::vector<double> adcscale;

  std::vector<int> layerlist;
  std::vector<int> numchinlayer;
  //  int numevent=1000;
  double DC_OFFSET=0;
  HistogramVariableStrage *hvs;
  InputInfo *info;
  TCanvas *c0;
  TCanvas *c1;
public:
  bool is_Strip(){return isStrip;}
  bool is_Pad(){return isPad;}
  TCanvas * getEventDisplayCanvas(){return c0;}
  TCanvas * getPulseShapeCanvas(){return c1;}

  LGADLoopManager(){}
  LGADLoopManager(UserInterFace _ui);
  ~LGADLoopManager(){}
  void SetCanvas();
  void SetInputList();
  void SetHistogram(HistogramVariableStrage *_hvs);
  void LoopEvent();
  unsigned int getNumChannel(){return filelist.size();}
  
  std::vector<int> getLayerList(){return layerlist;}
  std::vector<int> getNumChannelinLayer(){return numchinlayer;}
};
