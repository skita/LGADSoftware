#ifndef __InputInfo_hh__
#define __InputInfo_hh__
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <map>
#include "config.h"

class InputInfo{
public:
  std::string dirname;
  std::string filename;
  std::string flashadcch;
  int sensorchnum;
  int ampchnum;
  int layernum;
  double offsetlist;
  double timeoffset;
  double adcscale;
  std::map<std::string, double> adcscalemap;
  std::map<int, double> ampdelaymap;
  std::map<std::string, double> adcdelaymap;
  InputInfo(){
    std::stringstream ss; 
    ss.str("");
    ss<< PACKAGE_SOURCE_DIR << "paramter/ADCScale.txt" ;
    std::ifstream adcscalefile(ss.str().c_str());
    std::string str="";   std::string prestr="";   double num=0;
   while(!adcscalefile.eof()){
     adcscalefile >> str >> num;
     if(str==prestr)break;
//     std::cout << str << " " << num << std::endl;
     adcscalemap[str]=num;
     prestr=str;
   }
   ss.str("");
   ss<< PACKAGE_SOURCE_DIR << "paramter/AmpBoarddelay.txt" ;
   std::ifstream ampdelayfile(ss.str().c_str());
   int ch=0;   int prech=-1;   num=0;
   while(!ampdelayfile.eof()){
     ampdelayfile >> ch >> num;
     if(ch==prech)break;
//     std::cout << ch << " " << num << std::endl;
     ampdelaymap[ch]=num;
     prech=ch;
   }
   ss.str("");
   ss<< PACKAGE_SOURCE_DIR << "paramter/FlashADCdelay.txt" ;
   std::ifstream adcdelayfile(ss.str().c_str());
   str="";   prestr="";   num=0;
   while(!adcdelayfile.eof()){
     adcdelayfile >> str >> num;
     if(str==prestr)break;
//     std::cout << str << " " << num << std::endl;
     adcdelaymap[str]=num;
     prestr=str;
   }
  }
  ~InputInfo(){};
  void SetInputInfo(std::string x){
    filename=x.substr(0,x.find(" "));
    std::string fname=x.substr(x.find_last_of("/")+1);
    dirname=x.substr(0,x.find_last_of("/"));
    dirname=dirname.substr(dirname.find_last_of("/")+1);
    fname=fname.substr(0,fname.find(".txt"));
    flashadcch=fname;
    x=x.substr(x.find(" ")+1);
    while(x.find(" ")==0)x=x.substr(x.find(" ")+1);
    sensorchnum=stoi(x.substr(0,x.find(" ")));
    x=x.substr(x.find(" ")+1);
    while(x.find(" ")==0)x=x.substr(x.find(" ")+1);
    ampchnum=stoi(x.substr(0,x.find(" ")));
    x=x.substr(x.find(" ")+1);
    while(x.find(" ")==0)x=x.substr(x.find(" ")+1);
    layernum=stoi(x.substr(0,x.find(" ")));
    offsetlist=0;
    timeoffset=adcdelaymap[fname]+ampdelaymap[ampchnum];
    adcscale=adcscalemap[fname];

  }
  
};

#endif
