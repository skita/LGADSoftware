#include "LGADLoopManager.hh"

// Constructor (initialize this LGADLoopManager class object)
LGADLoopManager::LGADLoopManager(UserInterFace _ui){
  ui=_ui;
  debug = false;
  debug2 = false;
  eveprint=100;
  startevent=ui.getStartEvent();
  numevent=ui.getEndEvent();
  if(numevent<=0)numevent=999999999;
  isStrip = (ui.getJobType()=="strip")? true : false;
  isPad = (ui.getJobType()=="pad")? true : false;
  if(ui.getJobType()!="pad" && ui.getJobType()!="strip"){
    std::cout << "unkown jobtype : " << ui.getJobType() << std::endl;
  }
  cutth1=0.05;
  cutth2=0.05;
  SetCanvas();
  SetInputList();
}

// Setting InputList from UserInterFace 
void LGADLoopManager::SetInputList(){
  filelist.clear();
  info=new InputInfo();
  int ii=0;
  for(auto x : ui.getInputList()){
    if(debug)std::cout << x << std::endl;
    info->SetInputInfo(x);
    filelist.push_back(info->filename);
    flashadcnum.push_back(info->flashadcch);
    sensorchnum.push_back(info->sensorchnum);
    ampchnum.push_back(info->ampchnum);
    layernum.push_back(info->layernum);
    offsetlist.push_back(info->offsetlist);
    timeoffset.push_back(info->timeoffset);
    adcscale.push_back(info->adcscale);
    if(debug){
      std::cout << "filename       : " << filelist[ii] << std::endl;
      std::cout << "flash adc num  : " << flashadcnum[ii] << std::endl;
      std::cout << "sensor ch num  : " << sensorchnum[ii] << std::endl;
      std::cout << "amp ch num     : " << ampchnum[ii] << std::endl;
      std::cout << "layer num      : " << layernum[ii] << std::endl;
    }
    ii++;
  }
  // check how many layer?
  layerlist.clear();
  numchinlayer.clear();
  for(auto ln : layernum){
    bool isnewlay=true;
    int ilay=0;
    for(auto ll : layerlist){
      if(ll==ln){
	isnewlay=false;
	numchinlayer[ilay]++;
      }
      ilay++;
    }
    if(isnewlay){
      layerlist.push_back(ln);
      numchinlayer.push_back(1);
    }
  }
  std::cout << "total " << layerlist.size() << " layers are set" << std::endl;
  int ilay=0;
  for(auto nl : numchinlayer){
    std::cout << "layer " << layerlist[ilay] << " --> num of ch :   " << nl << std::endl;
    ilay++;
  }
  std::cout << " - - - -" << std::endl;
}

// Set Canvas for pulse height distribution of interesting events
void LGADLoopManager::SetCanvas(){
  // canvas for pulse shape check
  c0 = new TCanvas("c0","c0",800,1200);
  c0->Divide(1,3);

  c1 = new TCanvas("c1","c1",1200,1200);
  c1->Divide(2,4);
  int canpos=1;
  TH2D *frame0 = new TH2D("frame0","frame0",1,-1,6,1,-1.5,0.5);
  frame0->GetXaxis()->SetTitle("time [ns]");
  frame0->GetYaxis()->SetTitle("Pulse height [V]");
  TH2D *frame = new TH2D("frame","frame",1,-1,6,1,-1.7,0.5);
  frame->GetXaxis()->SetTitle("time [ns]");
  frame->GetYaxis()->SetTitle("Pulse height [V]");
  for(int ii=1;ii<=8;ii++){
    if(ii<4){
      c0->cd(ii);
      gPad->SetGridx(1);
      gPad->SetGridy(1);
      frame0->Draw();
    }
    c1->cd(ii);
    frame->Draw();
  }
  /////////////////////////////
}

// Preparation of Histograms
void LGADLoopManager::SetHistogram(HistogramVariableStrage *_hvs){
  hvs=_hvs;
  int ilay=0;
  for(auto ly : layerlist){
    std::string lystr="_ly"+std::to_string(ly); 
    for(int ii = 0 ;ii<numchinlayer[ilay];ii++){
      std::stringstream ss; ss.str("");
      ss << "ph_"<< ii << lystr;
      if(1){
	hvs->AddHistogram(ss.str(),100,0,1.2);
	hvs->GetHistogram(ss.str())->GetXaxis()->SetTitle("Pulse Height [V]");
      }else{
	hvs->AddHistogram(ss.str(),100,0,0.04);
	hvs->GetHistogram(ss.str())->GetXaxis()->SetTitle("Pulse Size [V#dotns]");
      }
      hvs->AddHistogram("ratio"+ss.str(),100,0,1.2);
      hvs->GetHistogram("ratio"+ss.str())->GetXaxis()->SetTitle("Pulse Height Ratio");
      hvs->AddHistogram("timediff"+ss.str(),100,-3,3);
      hvs->GetHistogram("timediff"+ss.str())->GetXaxis()->SetTitle("Time difference [ns]");
    }  
    hvs->AddHistogram("ph_next"+lystr,100,0,1.2);
    hvs->AddHistogram("ph_diag"+lystr,100,0,1.2);
    hvs->AddHistogram("phratio_next"+lystr,100,0,1.2);
    hvs->AddHistogram("phratio_diag"+lystr,100,0,1.2);
    hvs->Add2DHistogram("distphratio"+lystr,7,0.5,7.5,50,0,1.2);
    hvs->Get2DHistogram("distphratio"+lystr)->GetXaxis()->SetTitle("distance from leading strip [Strip]"); 
    hvs->Get2DHistogram("distphratio"+lystr)->GetYaxis()->SetTitle("Pulse Height Ratio to Leading strip"); 
    hvs->Add2DHistogram("disttime"+lystr,7,0.5,7.5,60,-2,2);
    hvs->Get2DHistogram("disttime"+lystr)->GetXaxis()->SetTitle("distance from leading strip [Strip]"); 
    hvs->Get2DHistogram("disttime"+lystr)->GetYaxis()->SetTitle("Arrival time delay from Leading strip"); 
    ilay++;
  }
  if(layerlist.size()==2){
    hvs->AddHistogram("layertimediff",100,-3,3);
    hvs->Add2DHistogram("lytdiffvsph",100,-3,3,50,0,0.5);
    for(int ii=0;ii<numchinlayer[0];ii++){
      for(int jj=0;jj<numchinlayer[1];jj++){
	std::stringstream ss; ss.str("");
	ss<<  ii << jj;
	hvs->AddHistogram("layertimediff"+ss.str(),100,-3,3);
	hvs->Add2DHistogram("lytdiffvsph"+ss.str(),100,-3,3,50,0,0.5);
      }
    }
  }
  hvs->AddHistogram("chisq",100,0,20);

}


// Event loop
void LGADLoopManager::LoopEvent(){
  ///////////////////////
  // opening all files 
  std::ifstream *ifslist;
  ifslist = new std::ifstream[filelist.size()];
  for(unsigned int ifile=0;ifile<filelist.size();ifile++)
    ifslist[ifile].open(filelist[ifile].c_str());
  ///////////////////////

  std::string line;
  bool isendoffile=false;

  ////////////////////////////////////////////////
  //
  /// start event loop
  int ipassedeve=0;
  int ieve=0;
  for(ieve=0;ieve<numevent;ieve++){
    //    if(ieve==2321 || ieve==2645 || ieve==2655) debug = true;
    //    else debug = false;

    // 2 dim vector for pulhei instance for each channel and each layer 
    std::vector<std::vector<PulseHeight>> pulhei; // pulhei[ich][ily]
    
    if(debug){
      std::cout << "--------------------------------------" << std::endl;
      std::cout << "       debug output enabled " << std::endl;
      std::cout << ""<< ieve << "th event started" << std::endl;
      std::cout << "--------------------------------------" << std::endl;
    }
    if(ieve%1000==0)std::cout << ieve << "th event started" << std::endl;

    //////////////////////////////////////////
    //
    // reading data from files  
    //
    double ADCcount[filelist.size()][1024];
    double ADCcounte[filelist.size()][1024];
    double time[1024];
    double timee[1024];
    std::vector<int> tmplaylist;
    for(unsigned int ifile=0;ifile<filelist.size();ifile++){
      if(ifslist[ifile].eof())isendoffile=true;
      if(debug)std::cout << "reading header" << std::endl;
      while(getline(ifslist[ifile],line)){
	if(debug2)std::cout << line << std::endl;
	if(line.find("Start Index Cell:")!=std::string::npos){
	  getline(ifslist[ifile],line);
	  break;
	}
      }
      if(debug)std::cout << "reading adc count" << std::endl;
      for(int ii=0;ii<1023;ii++){
	ADCcount[ifile][ii]=0;
	ifslist[ifile] >> ADCcount[ifile][ii];
	if(debug2)std::cout << ADCcount[ifile][ii] << std::endl;

	// offset correction
	ADCcount[ifile][ii]=ADCcount[ifile][ii]-2048*offsetlist[ifile]/100;
	ADCcount[ifile][ii]=(ADCcount[ifile][ii]-2048)/2048;
	ADCcounte[ifile][ii]=5./2048;
	time[ii]=ii*0.2;
	timee[ii]=0;
      }
      PulseHeight tmppulhei;
      tmppulhei.SetTimeADC(time,ADCcount[ifile],timee,ADCcounte[ifile]);
      tmppulhei.SetOffset(timeoffset[ifile],adcscale[ifile]);

      double mintime=0, minadc=100;
      double arrivaltime=0;
      tmppulhei.GetMinADC(mintime,minadc);
      tmppulhei.GetArrivalTime(arrivaltime);
      if(debug){
	std::cout << "eve" << ieve << " file " << ifile << " min adc : " << mintime << " " << minadc << std::endl;
	std::cout << "arrival time = " << arrivaltime << std::endl;
	std::cout << " min adc : " << mintime << " " << minadc << std::endl;
      }
      if(debug){
	std::cout << "filename       : " << filelist[ifile] << std::endl;
	std::cout << "layer num      : " << layernum[ifile] << std::endl;
      }

      //     
      // store tmppulhei to pulhei vector-vector 
      //
      bool isnewlayer=true;
      int layid=0;
      for(auto x : tmplaylist){
	if(x==layernum[ifile]){
	  isnewlayer=false;
	  break;
	}
	layid++;
      }
      if(debug)std::cout  << "layid : "<< layid << std::endl;
      if(isnewlayer){
	tmplaylist.push_back(layid);
	std::vector<PulseHeight> tmpphlist;
	tmpphlist.push_back(tmppulhei);
	pulhei.push_back(tmpphlist);
      }else{
	pulhei[layid].push_back(tmppulhei);
      }
      if(debug){
	std::cout << "nlay : " << tmplaylist.size() << std::endl;
	std::cout << "pulhei layer? " << pulhei.size() << std::endl;
	for(auto x :pulhei){
	  std::cout << x.size() << std::endl;
	}
      }
    } // read file done.
    //////////////////////////////////////////


    //////////////////////////////////////////
    // check pulse height and re-order by height
    std::vector<int> maxid;maxid.clear();
    std::vector<std::vector<int>> idorder; idorder.clear();
    int ilay=0;
    for(auto ly : layerlist){
      
      int tmpmaxid=-1;
      // get pulse height(minimum point) for all channel
      std::vector<double> phlist;phlist.clear();
      for(unsigned int ii =0;ii<pulhei[ilay].size();ii++){
	phlist.push_back(pulhei[ilay][ii].GetPulseHeight(1));
      }
      
      //  sort by pulse height
      std::vector<double> phsort;phsort.clear();
      for(auto x:phlist)phsort.push_back(x);
      std::sort(phsort.begin(),phsort.end(),std::greater<double>());
      //    if(phsort[ilay]>-0.5)continue;
      
      // make vector for order of pulse height
      std::vector<int> tmpidorder; tmpidorder.clear();
      for(unsigned int ii=0;ii<phlist.size();ii++){
	for(unsigned int jj=0;jj<phsort.size();jj++){
	  if(phlist[ii]==phsort[jj])tmpidorder.push_back(jj);
	}
      }
      
      // get id for maximum pulse height
      for(unsigned int ii=0;ii<pulhei[ilay].size();ii++){
	if(tmpidorder[ii]==0){
	  tmpmaxid=ii;
	}
      }
      // to make time=0 as arrival time of leading signal of layer 0
      for(unsigned int ii=0;ii<pulhei[ilay].size();ii++){
	if(ilay==0)pulhei[ilay][ii].SetOffset(-1*pulhei[0][tmpmaxid].GetTimeArrival());
	else pulhei[ilay][ii].SetOffset(-1*pulhei[0][maxid[0]].GetTimeArrival());
	if(debug)std::cout << phlist[ii] << " " << tmpidorder[ii] << std::endl; // 
      }
      maxid.push_back(tmpmaxid);
      idorder.push_back(tmpidorder);

      for(unsigned int ii=0;ii<pulhei[ilay].size();ii++){
	if(pulhei[ilay][ii].GetPulseHeight(1)<0.05)continue;
	if(debug)pulhei[ilay][ii].PrintADCcount();
	pulhei[ilay][ii].MakeGraph();
	pulhei[ilay][ii].FitGraph();
	if(debug){
	  std::cout << pulhei[ilay][ii].GetFitResultPtr() << std::endl;
	  if(ilay==0&&ii==tmpmaxid)std::cout << pulhei[ilay][ii].GetFitChisq() << " " <<pulhei[ilay][ii].GetPulseHeight(4)/pulhei[ilay][ii].GetPulseHeight(1) << std::endl;

	}

	// more pulse height analysis here...

      }

      ilay++;
    }
    //////////////////////////////////////////
    
    
    //////////////////////////////////////////
    // filling histograms 
    int icolor=1;
    if(ieve>=startevent){

      /////////////////////////////////////////      
      // event select condition for filling histogram
      bool passSelection=true;
      // if leading pulse height < 100mV
      if(pulhei[0][maxid[0]].GetPulseHeight()<cutth1)passSelection=false;
      // in case stack, second layer leading pulse height < 50mV
      if(pulhei.size()>=2){
	if(pulhei[1][maxid[1]].GetPulseHeight()<cutth2)passSelection=false;
      }
      /////////////////////////////////////////      

      if(passSelection){
	int ilay=0;
	for(auto ly : layerlist){
	  std::string lystr="_ly"+std::to_string(ly); 
	  
	  for(int ii=0;ii<pulhei[ilay].size();ii++){
	    std::stringstream ss; ss.str("");
	    ss << "ph_"<< idorder[ilay][ii] << lystr;
	    
	    hvs->GetHistogram(ss.str())->Fill(pulhei[ilay][ii].GetPulseHeight());
	    hvs->GetHistogram("ratio"+ss.str())->Fill(pulhei[ilay][ii].GetPulseHeight()/pulhei[ilay][maxid[ilay]].GetPulseHeight());
	    if(ii!=maxid[ilay]){
	      if(fabs(maxid[ilay]-(int)ii)==2){
		hvs->GetHistogram("ph_diag"+lystr)->Fill(pulhei[ilay][ii].GetPulseHeight());
		hvs->GetHistogram("phratio_diag"+lystr)->Fill(pulhei[ilay][ii].GetPulseHeight()/pulhei[ilay][maxid[ilay]].GetPulseHeight());
	      }else {
		hvs->GetHistogram("ph_next"+lystr)->Fill(pulhei[ilay][ii].GetPulseHeight());
		hvs->GetHistogram("phratio_next"+lystr)->Fill(pulhei[ilay][ii].GetPulseHeight()/pulhei[ilay][maxid[ilay]].GetPulseHeight());
	      }
	    }
	    hvs->GetHistogram("timediff"+ss.str())->Fill(pulhei[ilay][ii].GetTimeArrival()-pulhei[ilay][maxid[ilay]].GetTimeArrival());
	    hvs->Get2DHistogram("distphratio"+lystr)->Fill(fabs(maxid[ilay]-(int)ii),pulhei[ilay][ii].GetPulseHeight()/pulhei[ilay][maxid[ilay]].GetPulseHeight());
	    hvs->Get2DHistogram("disttime"+lystr)->Fill(fabs(maxid[ilay]-(int)ii),pulhei[ilay][ii].GetTimeArrival()-pulhei[ilay][maxid[ilay]].GetTimeArrival());
	  
	    int num=-1;
	    for(unsigned int jj=0;jj<pulhei[ilay].size();jj++){
	      if(ii==idorder[ilay][jj])num=jj;
	    }
	    if(debug)std::cout << ii << " " << pulhei[ilay][num].GetPulseHeight() << std::endl;

	    hvs->GetHistogram("chisq")->Fill(pulhei[ilay][ii].GetFitChisq());
	    if(ipassedeve<eveprint){
	      pulhei[ilay][ii].MakeGraph();
	      if(ipassedeve<3){
		pulhei[ilay][ii].FitGraph(true);
		if(debug)pulhei[ilay][ii].PrintADCcount();
		c0->cd(ipassedeve+1);
		pulhei[ilay][ii].GetGraph()->SetMarkerColor(icolor);
		pulhei[ilay][ii].GetGraph()->SetLineColor(icolor);
		pulhei[ilay][ii].GetFitfunc()->SetMarkerColor(icolor);
		pulhei[ilay][ii].GetFitfunc()->SetLineColor(icolor++);
		pulhei[ilay][ii].GetGraph()->Draw("PSame");
		pulhei[ilay][ii].GetFitfunc()->Draw("Same");
		c0->Update();
	      }else{
		c1->cd(4*ilay+idorder[ilay][ii]+1);
		pulhei[ilay][ii].GetGraph()->Draw("LSame");
		c1->Update();
	      }
	    }
	  }
	  ilay++;
	}

	if(layerlist.size()==2){
	  hvs->GetHistogram("layertimediff")->Fill(pulhei[0][maxid[0]].GetTimeArrival()-pulhei[1][maxid[1]].GetTimeArrival());
	  hvs->Get2DHistogram("lytdiffvsph")->Fill(pulhei[0][maxid[0]].GetTimeArrival()-pulhei[1][maxid[1]].GetTimeArrival(),
						   std::min(pulhei[0][maxid[0]].GetPulseHeight(),pulhei[1][maxid[1]].GetPulseHeight()));
	  //	  if(maxid[0]==0&&maxid[1]==0)
	  //	    hvs->GetHistogram("layertimediff00")->Fill(pulhei[0][maxid[0]].GetTimeArrival()-pulhei[1][maxid[1]].GetTimeArrival());
	  std::stringstream ss00; ss00.str();
	  ss00 <<  maxid[0] << maxid[1];
	  hvs->GetHistogram("layertimediff"+ss00.str())->Fill(pulhei[0][maxid[0]].GetTimeArrival()-pulhei[1][maxid[1]].GetTimeArrival());
	  hvs->Get2DHistogram("lytdiffvsph"+ss00.str())->Fill(pulhei[0][maxid[0]].GetTimeArrival()-pulhei[1][maxid[1]].GetTimeArrival(),
							      std::min(pulhei[0][maxid[0]].GetPulseHeight(),pulhei[1][maxid[1]].GetPulseHeight()));
	}

	ipassedeve++;
      }
    }
    if(debug)std::cout << ieve << "th event processed" << std::endl;
    if(isendoffile)break;
  }
  std::cout << "Total " << ieve << " event processed" << std::endl;
  std::cout << "Passed Event : " << ipassedeve << " event" << std::endl;
  int ilay=0;
  for(auto ly : layerlist){
    std::string lystr="_ly"+std::to_string(ly); 
    std::cout << "---- ly" << ilay << " ------" << std::endl;
    std::cout << "small " << hvs->GetHistogram("ph_0"+lystr)->GetMean() << std::endl;
    std::stringstream ss;ss.str("");
    ss << "ph_" <<numchinlayer[ilay]-1 << lystr;
    std::cout << "large " << hvs->GetHistogram(ss.str())->GetMean() << std::endl;
    std::cout << "ratio " << hvs->GetHistogram("ph_0"+lystr)->GetMean()/hvs->GetHistogram(ss.str())->GetMean() << std::endl;
    ilay++;
  }

}
