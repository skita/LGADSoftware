#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include "AtlasStyle.hh"
#include "InputInfo.h"
#include "PulseHeight.h"
#include "UserInterFace.hh"
#include "HistogramVariableStrage.hh"
#include "LGADLoopManager.hh"
#include "TH2D.h"
#include "TGraphErrors.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TLegend.h"
#include "TFile.h"

void DrawPlotToCanvas(LGADLoopManager llm,HistogramVariableStrage *hvs,std::string sufix); 


int main(int argc, char *argv[]){
  
  UserInterFace ui;
  ui.SetCondition();
  
  std::cerr << "INFO : " << ui.getInputList().size()
            << " Files are loaded." << std::endl;

  SetAtlasStyle();

  HistogramVariableStrage *hvs = new HistogramVariableStrage("");
  LGADLoopManager llm(ui);
  llm.SetHistogram(hvs);
  llm.LoopEvent();
  TFile *fp = new TFile((ui.getOutputFilename()).c_str(),"RECREATE");
  for(auto x : hvs->GetHistName()) hvs->GetHistogram(x)->Write();
  for(auto x : hvs->Get2DHistName()) hvs->Get2DHistogram(x)->Write();
  fp->Close();

  ///////////  if plot is neccesarry  //////////////
  std::string sufix=ui.getOutputFilename();
  DrawPlotToCanvas(llm,hvs,sufix);
  return 0 ; 
}



void DrawPlotToCanvas(LGADLoopManager llm,HistogramVariableStrage *hvs,std::string sufix){
  sufix=sufix.substr(sufix.find("/")+1);
  sufix=sufix.substr(6);
  sufix=sufix.substr(0,sufix.find("."));
  llm.getEventDisplayCanvas()->Print(("results/event_"+sufix+".png").c_str());
  llm.getEventDisplayCanvas()->Print(("results/event_"+sufix+".pdf").c_str());
  llm.getPulseShapeCanvas()->Print(("results/pulseshape_"+sufix+".png").c_str());
  llm.getPulseShapeCanvas()->Print(("results/pulseshape_"+sufix+".pdf").c_str());


  int ilay=0;
  for(auto ly : llm.getLayerList()){
    std::string lystr="_ly"+std::to_string(ly); 

    TCanvas *c2 = new TCanvas("c2","c2",1500,1000);
    c2->Divide(3,2);

    double ymaxph=0;
    double ymaxphratio=0;
    double ymaxdiff=0;

    for(unsigned int ii=0;ii<llm.getNumChannelinLayer()[ilay];ii++){
      std::stringstream ss; ss.str("");
      ss<<"ph_" << ii << lystr;
      hvs->GetHistogram(ss.str())->Rebin(2);
      hvs->GetHistogram("ratio"+ss.str())->Rebin(2);
      hvs->GetHistogram("timediff"+ss.str())->Rebin(2);

      if(ymaxph<hvs->GetHistogram(ss.str())->GetMaximum())ymaxph=hvs->GetHistogram(ss.str())->GetMaximum();
      if(ii!=0&&ymaxphratio<hvs->GetHistogram("ratio"+ss.str())->GetMaximum())ymaxphratio=hvs->GetHistogram("ratio"+ss.str())->GetMaximum();
      if(ii!=0&&ymaxdiff<hvs->GetHistogram("timediff"+ss.str())->GetMaximum())ymaxdiff=hvs->GetHistogram("timediff"+ss.str())->GetMaximum();
    }


    std::map<int,std::string> countstr;
    countstr[1]="st";
    countstr[2]="nd";
    countstr[3]="rd";

    TLegend *leg0=new TLegend(0.7,0.7,0.9,0.9);
    leg0->SetBorderSize(0);
    leg0->SetFillStyle(0);
    for(unsigned int ii=0;ii<llm.getNumChannelinLayer()[ilay];ii++){
      std::stringstream ss0; ss0.str("");
      ss0 << "ph_" << ii <<lystr;
      std::stringstream ss; ss.str("");
      if((ii+1)%10>=1&&(ii+1)%10<=3) ss<< ii<< countstr[ii] << " highest";
      else ss<< ii<< "th highest";
      leg0->AddEntry(hvs->GetHistogram(ss0.str()),ss.str().c_str(),"l");
    }

    TLegend *leg1=new TLegend(0.7,0.7,0.9,0.9);
    leg1->SetBorderSize(0);
    leg1->SetFillStyle(0);

    for(unsigned int ii=1;ii<llm.getNumChannelinLayer()[ilay];ii++){
      std::stringstream ss0; ss0.str("");
      ss0 << "ph_" << ii << lystr;
      std::stringstream ss; ss.str("");
      if((ii+1)%10>=1&&(ii+1)%10<=3) ss<< ii<< countstr[ii] << " highest";
      else ss<< ii<< "th highest";
      leg1->AddEntry(hvs->GetHistogram(ss0.str()),ss.str().c_str(),"l");
    }


    for(unsigned int ii=0;ii<llm.getNumChannelinLayer()[ilay];ii++){
      std::stringstream ss0; ss0.str("");
      ss0 << "ph_" << ii << lystr;
      hvs->GetHistogram(ss0.str())->SetLineColor(ii+2);
      hvs->GetHistogram(ss0.str())->SetMaximum(ymaxph*1.2);
      hvs->GetHistogram(("ratio"+ss0.str()))->SetLineColor(ii+2);
      hvs->GetHistogram(("ratio"+ss0.str()))->SetMaximum(ymaxphratio*1.2);

      c2->cd(1);
      if(ii==0)hvs->GetHistogram(ss0.str())->Draw("HIST");
      else hvs->GetHistogram(ss0.str())->Draw("SameHIST");
      c2->cd(2);
      if(ii==1)hvs->GetHistogram(("ratio"+ss0.str()))->Draw("HIST");
      else if(ii>1)hvs->GetHistogram(("ratio"+ss0.str()))->Draw("SameHIST");
    }
    if(llm.is_Strip()){
      c2->cd(4);
      hvs->Get2DHistogram("distphratio"+lystr)->Draw("COLZ");
      c2->cd(5);
      hvs->Get2DHistogram("disttime"+lystr)->Draw("COLZ");
    }
    if(llm.is_Pad()){
      c2->cd(4);
    
      hvs->GetHistogram("ph_next"+lystr)->Scale(1./hvs->GetHistogram("ph_next"+lystr)->Integral());
      hvs->GetHistogram("ph_next"+lystr)->SetLineColor(38);
      hvs->GetHistogram("ph_diag"+lystr)->Scale(1./hvs->GetHistogram("ph_diag"+lystr)->Integral());
      hvs->GetHistogram("ph_diag"+lystr)->SetLineColor(46);
      TLegend *leg2 = new TLegend(0.7,0.7,0.9,0.9);
      leg2->AddEntry(hvs->GetHistogram("ph_0"+lystr),"Leading PAD", "l");
      leg2->AddEntry(hvs->GetHistogram("ph_next"+lystr),"Next PAD", "l");
      leg2->AddEntry(hvs->GetHistogram("ph_diag"+lystr),"Diagonal PAD", "l");
      hvs->GetHistogram("ph_diag"+lystr)->Draw("HIST");
      hvs->GetHistogram("ph_next"+lystr)->Draw("HISTSame");
      TH1D * h_phlead=new TH1D(*(TH1D*)hvs->GetHistogram("ph_0"+lystr)->Clone());
      h_phlead->Scale(1./h_phlead->Integral());
      h_phlead->Draw("HISTSame");
      leg2->Draw("Same");

      c2->cd(5);
      hvs->GetHistogram("phratio_next"+lystr)->Scale(1./hvs->GetHistogram("phratio_next"+lystr)->Integral());
      hvs->GetHistogram("phratio_next"+lystr)->SetLineColor(38);
      hvs->GetHistogram("phratio_diag"+lystr)->Scale(1./hvs->GetHistogram("phratio_diag"+lystr)->Integral());
      hvs->GetHistogram("phratio_diag"+lystr)->SetLineColor(46);
      TLegend *leg3 = new TLegend(0.7,0.7,0.9,0.9);
      leg3->AddEntry(hvs->GetHistogram("phratio_next"+lystr),"Next PAD", "l");
      leg3->AddEntry(hvs->GetHistogram("phratio_diag"+lystr),"Diagonal PAD", "l");
      hvs->GetHistogram("phratio_diag"+lystr)->Draw("HIST");
      hvs->GetHistogram("phratio_next"+lystr)->Draw("HISTSame");
      leg3->Draw("Same");
    }
  
    c2->cd(1);
    leg0->Draw("Same");
    c2->cd(2);
    leg1->Draw("Same");
    c2->cd(6);
    //  h_timediffminmax->Draw();
    for(unsigned int ii=1;ii<llm.getNumChannelinLayer()[ilay];ii++){
      std::stringstream ss; ss.str("");
      ss << "ph_" << ii << lystr;
      hvs->GetHistogram("timediff"+ss.str())->SetLineColor(ii+2);
      hvs->GetHistogram("timediff"+ss.str())->SetMaximum(ymaxdiff*1.2);
      hvs->GetHistogram("timediff"+ss.str())->Draw("HISTSame");
    }
    leg1->Draw("Same");

    ilay++;
    c2->Update();
    c2->Print(("results/plots_"+sufix+lystr+".png").c_str());
    c2->Print(("results/plots_"+sufix+lystr+".pdf").c_str());
    delete c2;
  }

  if(llm.getLayerList().size()==2){
    TCanvas *c3 = new TCanvas("c3","c3",800,800);
    TF1* ga=new TF1("ga","gaus");
    ga->SetParameter(1,0);
    TFitResultPtr r = hvs->GetHistogram("layertimediff")->Fit("ga");
    hvs->GetHistogram("layertimediff")->Draw();
    std::cout << "fit result ptr = " << r << " -->  "  << "total" << " : " << ga->GetParameter(2) << "+-" << ga->GetParError(2) << std::endl;
    int kk=0;
    double nume=0;
    double deno=0;
    for(int ii=0;ii<llm.getNumChannelinLayer()[0];ii++){
      for(int jj=0;jj<llm.getNumChannelinLayer()[1];jj++){
	std::stringstream ss; ss.str("");
	ss<< "layertimediff" << ii << jj;
	TF1* ga=new TF1(ss.str().c_str(),"gaus");
	ga->SetParameter(1,0);
	hvs->GetHistogram(ss.str())->SetMarkerColor(kk+2);
	hvs->GetHistogram(ss.str())->SetLineColor(kk+2);
	hvs->GetHistogram(ss.str())->Draw("Same");
	TFitResultPtr r = hvs->GetHistogram(ss.str())->Fit(ss.str().c_str(),"Q","");
	ga->Draw("Same");
	std::cout << "fit result ptr = " << r << " -->  "  << ss.str() << " : " << ga->GetParameter(2) << "+-" << ga->GetParError(2) << std::endl;;
	nume+= ga->GetParameter(2)/ga->GetParError(2)/ga->GetParError(2);
	deno+= 1/ga->GetParError(2)/ga->GetParError(2);
	kk++;
      }
    }
    std::cout << "Corrected sigma(T2-T1)     : " << nume/deno << "+-" << 1./sqrt(deno) << std::endl;
    std::cout << "Corrected time resolution  : " << nume/deno/sqrt(2) << "+-" << 1./sqrt(deno)/sqrt(2) << std::endl;



    //    hvs->GetHistogram("layertimediff00")->SetMarkerColor(2);
    //    hvs->GetHistogram("layertimediff00")->SetLineColor(2);
    //    hvs->GetHistogram("layertimediff00")->Draw("Same");
    c3->Print(("results/timediff_"+sufix+".png").c_str());
    c3->Print(("results/timediff_"+sufix+".pdf").c_str());
  } 
}
